/*
 * ao_sys.c
 *
 *  Created on: Dec 3, 2023
 *      Author: thony
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "driver.h"
#include "ao_sys.h"
#include "ao_led.h"

//--------------Objeto activo ao_led--------------------

static ao_led_t ao_led_t_; //Objeto activo
static Event_led_t Event_led_t_; // Evento del objeto activo sys

//                  Callback ao_led

void ao_led_callbak_t_(ao_led_t *hao, Event_led_t state)//se le pasa un elemento , ej, Event_led_t[0]
{
	ELOG("Callback, LED 1: %d , mode: %d",state[0].led_t,state[0].shape_t);
	ELOG("Callback, LED 2: %d , mode: %d",state[1].led_t,state[1].shape_t);
}
//--------------Procesamiento del objeto activo sys



static void task_(void* argument)
{
	ao_sys_t* hao= (ao_sys_t*)argument;
	ao_led_init(&ao_led_t_, ao_led_callbak_t_);//Inicializacion objeto activo led
	// INICIALIZAMOS LOS LED EN OFF
	Event_led_t_[0].led_t= RED;
	Event_led_t_[1].led_t= GREEN;
	Event_led_t_[0].shape_t= OFF;
	Event_led_t_[1].shape_t= OFF;
	while(true)
	{
		event_button_t event;
		if(pdPASS == xQueueReceive(hao->hqueue, &event, portMAX_DELAY))
		{
		  ELOG("ao, sys:%d", event);
		  hao->callback(hao, event);
		}
		//Procesar los datos de la cola del objeto button
		update_ao_led_event(&event);
		//Producir cola con los resultados del procesamiento de datos
		ao_led_send(&ao_led_t_, Event_led_t_);


	}

}


void ao_sys_init(ao_sys_t* hao, ao_sys_callbak_t callback)
{
	hao->callback = callback;
	hao->hqueue = xQueueCreate(QUEUE_LENGTH_SYS, QUEUE_ITEM_SIZE_SYS);
	while(NULL == hao->hqueue){}
	BaseType_t status;
	status = xTaskCreate(task_, "task_ao_sys", 128, (void* const)hao, (tskIDLE_PRIORITY + 2UL), NULL);
		while (pdPASS != status)
		{
		    // error
		}
}

bool ao_sys_send(ao_sys_t* hao, event_button_t state)
{
	return (pdPASS == xQueueSend(hao->hqueue, (void*)&state, 0));

}

// Procesamiento del objeto activo

void update_ao_led_event(event_button_t* event)
{
	switch (*event) {
		case INICIAL:
			Event_led_t_[0].led_t= RED;
			Event_led_t_[1].led_t= GREEN;
			Event_led_t_[0].shape_t= OFF;
			Event_led_t_[1].shape_t= OFF;

			break;
		case CORTO:
			Event_led_t_[0].led_t= RED;
			Event_led_t_[0].shape_t= OFF;
			Event_led_t_[1].led_t= GREEN;
			Event_led_t_[1].shape_t= BLINK;
			break;
		case LARGO:
			Event_led_t_[0].led_t= RED;
			Event_led_t_[0].shape_t= BLINK;
			Event_led_t_[1].led_t= GREEN;
			Event_led_t_[1].shape_t= OFF;
			break;
		case TRABADO:
			Event_led_t_[0].led_t= RED;
			Event_led_t_[0].shape_t= ON;
			Event_led_t_[1].led_t= GREEN;
			Event_led_t_[1].shape_t= ON;

			break;
		default:
			Event_led_t_[0].led_t= RED;
			Event_led_t_[1].led_t= GREEN;
			Event_led_t_[0].shape_t= OFF;
			Event_led_t_[1].shape_t= OFF;

			break;
	}
}
