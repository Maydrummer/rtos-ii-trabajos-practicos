/*
 * ao_button.c
 *
 *  Created on: Dec 3, 2023
 *      Author: thony
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "driver.h"
#include "ao_button.h"
#include "ao_sys.h"

//--------------Objeto activo ao_sys--------------------

static ao_sys_t ao_sys_t_; //Objeto activo
static event_button_t event_button_t_; // Evento del objeto activo sys

//                  Callback ao_sys

void ao_sys_callbak_t_(ao_sys_t *hao, event_button_t state)
{
	ELOG("Callback, sys: %d", state);

}

// PROCESAMIENTO DEL OBJETO ACTIVO BUTTON
static uint32_t time_init;
static uint32_t time_pressed;



static void task_(void* argument)
{
	ao_button_t* hao= (ao_button_t*)argument;
	ao_sys_init(&ao_sys_t_, ao_sys_callbak_t_);//Inicializacion objeto activo sys
	event_button_t_ = INICIAL;
	time_init = 0;
	while(true)
	{
		event_t_ event;
		// Consumir cola del estado del boton
		if(pdPASS == xQueueReceive(hao->hqueue, &event, portMAX_DELAY))
		{
		  ELOG("ao, button:%d", event.state);
		  hao->callback(hao, event.state);
		}
		//Procesar los datos de la cola del estado del boton
		ao_button_time_pressed(&event);
		ELOG("ao, button, time pressed: %lu",time_pressed);
		update_ao_sys_event();
		//Producir cola con los resultados del procesamiento de datos
		ao_sys_send(&ao_sys_t_ , event_button_t_ );

	}

}

// INICIALZACION DEL OBJETO ACTIVO
void ao_button_init(ao_button_t* hao, ao_button_callbak_t callback)
{
	hao->callback = callback;
	hao->hqueue = xQueueCreate(QUEUE_LENGTH_, QUEUE_ITEM_SIZE_);
	while(NULL == hao->hqueue){}
	BaseType_t status;
	status = xTaskCreate(task_, "task_ao_button", 128, (void* const)hao, (tskIDLE_PRIORITY + 2UL), NULL);
	while (pdPASS != status)
	{
	    // error
	}
}

// NOTIFICACION DEL OBJETO ACTIVO
bool ao_button_send(ao_button_t* hao, bool state)
{
	event_t_ event;
	event.state = state;
	return (pdPASS == xQueueSend(hao->hqueue, (void*)&event, 0));
}

// PROCESAMIENTO DEL OBJETO ACTIVO

void ao_button_time_pressed(event_t_* event)
{
	if((event->state == true) && (time_init == (uint32_t) 0 ))
	{
		time_init = HAL_GetTick();
	}
	else if( (event->state == false) && (time_init > (uint32_t) 0 ) )
	{
		time_pressed = HAL_GetTick()- time_init;
		time_init = 0;
	}

}

void update_ao_sys_event()
{
	if ( (t_corto <= time_pressed) && (time_pressed < t_largo))
	{
		event_button_t_= CORTO;

	}
	else if((t_largo <= time_pressed) && (time_pressed < t_trabado))
	{
		event_button_t_= LARGO;
	}
	else if(time_pressed > t_trabado)
	{
		event_button_t_= TRABADO;
	}
	else
	{
		event_button_t_= INICIAL;
	}

}
