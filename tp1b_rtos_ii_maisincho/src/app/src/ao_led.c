/*
 * ao_led.c
 *
 *  Created on: Dec 3, 2023
 *      Author: thony
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "driver.h"
#include "ao_led.h"


static void task_(void* argument)
{
	ao_led_t* hao= (ao_led_t*)argument;
	while(true)
	{
		Event_led_t event;
		if(pdPASS == xQueueReceive(hao->hqueue, &event, portMAX_DELAY))
		{
		  hao->callback(hao, event);
		}

	}
}

void ao_led_init(ao_led_t* hao, ao_led_callbak_t callback)
{
	hao->callback = callback;
	hao->hqueue = xQueueCreate(QUEUE_LENGTH_LED, QUEUE_ITEM_SIZE_LED);
	while(NULL == hao->hqueue){}
	BaseType_t status;
	status = xTaskCreate(task_, "task_ao_led", 128, (void* const)hao, (tskIDLE_PRIORITY + 2UL), NULL);
	while (pdPASS != status)
	{
	    // error
	}

}

bool ao_led_send(ao_led_t* hao, Event_led_t state)
{
	return (pdPASS == xQueueSend(hao->hqueue, (void*)state,0));//como es un arreglo, no se pone nuevamente el signo & de direccion

}
