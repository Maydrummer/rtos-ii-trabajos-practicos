/*
 * ao_button.h
 *
 *  Created on: Dec 3, 2023
 *      Author: thony
 */

#ifndef APP_INC_AO_BUTTON_H_
#define APP_INC_AO_BUTTON_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
// COLA DEL OBJETO ACTIVO
#define QUEUE_LENGTH_            (5)
#define QUEUE_ITEM_SIZE_         (sizeof(event_t_))
// TIPO DE EVENTO DEL OBJETO ACTIVO
typedef struct
{
  bool state;
} event_t_;


#define t_corto    100
#define t_largo    2000
#define t_trabado  8000

/* CREACION DEL OBJETO ACTIVO EN UNA ESTRUCTURA : NOTIFICACION(COLA) + CALLBACK */
typedef struct ao_button_s ao_button_t;

typedef void (*ao_button_callbak_t)(ao_button_t *hao, bool state);

struct ao_button_s
{
    QueueHandle_t hqueue;
    ao_button_callbak_t callback;
};


/* PROTOTIPOS DE FUNCIONES */
bool ao_button_send(ao_button_t* hao, bool state);

void ao_button_init(ao_button_t* hao, ao_button_callbak_t callback);

void ao_button_time_pressed(event_t_* event);

void update_ao_sys_event();

#endif /* APP_INC_AO_BUTTON_H_ */
