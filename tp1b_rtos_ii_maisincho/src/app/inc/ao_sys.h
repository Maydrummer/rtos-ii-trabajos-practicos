/*
 * ao_sys.h
 *
 *  Created on: Dec 3, 2023
 *      Author: thony
 */

#ifndef APP_INC_AO_SYS_H_
#define APP_INC_AO_SYS_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define QUEUE_LENGTH_SYS            (5)
#define QUEUE_ITEM_SIZE_SYS         (sizeof(event_button_t))
#define t_corto    100
#define t_largo    2000
#define t_trabado  8000
// TIPO DE EVENTO DEL OBJETO ACTIVO
typedef enum
{
	INICIAL,
	CORTO,
	LARGO,
	TRABADO,
}event_button_t;

/* CREACION DEL OBJETO ACTIVO EN UNA ESTRUCTURA : NOTIFICACION(COLA) + CALLBACK */
typedef struct ao_sys_s ao_sys_t;

typedef void (*ao_sys_callbak_t)(ao_sys_t *hao, event_button_t state);

struct ao_sys_s
{
    QueueHandle_t hqueue;
    ao_sys_callbak_t callback;
};

/* PROTOTIPOS DE FUNCIONES */
bool ao_sys_send(ao_sys_t* hao, event_button_t state);

void ao_sys_init(ao_sys_t* hao, ao_sys_callbak_t callback);
void update_ao_led_event(event_button_t* event);

#endif /* APP_INC_AO_SYS_H_ */
