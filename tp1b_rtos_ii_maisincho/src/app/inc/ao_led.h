/*
 * ao_led.h
 *
 *  Created on: Dec 3, 2023
 *      Author: thony
 */

#ifndef APP_INC_AO_LED_H_
#define APP_INC_AO_LED_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>



// TIPO DE EVENTO DEL OBJETO ACTIVO

typedef enum
{
	OFF,
	BLINK,
	ON,
}Shape;

typedef enum
{
	RED,
	GREEN,
}Led;

typedef struct
{
	Shape   shape_t;
	Led		led_t;
}Event_led;

typedef Event_led Event_led_t[2];

#define QUEUE_LENGTH_LED            (5)
#define QUEUE_ITEM_SIZE_LED         (sizeof(Event_led_t))

// CREACION DEL OBJETO ACTIVO EN UNA ESTRUCTURAl: NOTIFICACION (COLA) + CALLBACK
typedef struct ao_led_s ao_led_t;

typedef void (*ao_led_callbak_t)(ao_led_t *hao, Event_led_t state);

struct ao_led_s
{
    QueueHandle_t hqueue;
    ao_led_callbak_t callback;
};

/* PROTOTIPOS DE FUNCIONES */
bool ao_led_send(ao_led_t* hao, Event_led_t state);

void ao_led_init(ao_led_t* hao, ao_led_callbak_t callback);

#endif /* APP_INC_AO_LED_H_ */
