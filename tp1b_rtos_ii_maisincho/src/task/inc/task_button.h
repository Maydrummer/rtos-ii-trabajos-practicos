/*
 * task_button.h
 *
 *  Created on: Nov 12, 2023
 *      Author: thony
 */

#ifndef TASK_INC_TASK_BUTTON_H_
#define TASK_INC_TASK_BUTTON_H_

typedef enum{
BUTTON_UP,
BUTTON_FALLING,
BUTTON_DOWN,
BUTTON_RAISING,
}debounceState_t;

void debounceFSM_init();		// debe cargar el estado inicial
void debounceFSM_update();	// debe leer las entradas, resolver la lógica de
void task_button(void* argument);


#endif /* TASK_INC_TASK_BUTTON_H_ */
