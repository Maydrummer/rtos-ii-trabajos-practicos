/*
 * task_button.c
 *
 *  Created on: Nov 12, 2023
 *      Author: thony
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "driver.h"
#include "task_button.h"
#include "ao_button.h"


//---------------------------- Sistemas Reactivos TP1 B----------------------------------------
//Integrantes: Anthony Maisincho Jivaja, Jesus Gonzales Davila

#define ANTIREBOTE_TIME 40  // Tiempo de antirrebote en milisegundos
#define TASK_INTERVAL 20    // Intervalo de la tarea en milisegundos

static uint32_t time_aux;
static ao_button_t ao_button_; //Objeto activo button
static bool button_state_; // Evento del objeto activo ao_button
static debounceState_t debounce_State_t_;

void debounceFSM_init()
{
	debounce_State_t_=BUTTON_UP;
	time_aux = 0;
	button_state_ = false;//Boton no presionado

}

void debounceFSM_update()
{
	switch (debounce_State_t_) {
		case BUTTON_UP:
			if(eboard_switch()== GPIO_PIN_RESET)
			{
				debounce_State_t_ = BUTTON_FALLING;
				time_aux= HAL_GetTick();
			}
			break;
		case BUTTON_FALLING:
			if((HAL_GetTick()- time_aux)>= ANTIREBOTE_TIME)
			{
				if(  eboard_switch()== GPIO_PIN_RESET )
				{
					button_state_ = true;
					debounce_State_t_ = BUTTON_DOWN; // Boton presionado
					time_aux = 0;
				}
				else
				{
					debounce_State_t_=BUTTON_UP;
				}
			}

			break;
		case BUTTON_DOWN:
			if(eboard_switch()== GPIO_PIN_SET)
			{
				debounce_State_t_ = BUTTON_RAISING;
				time_aux= HAL_GetTick();
			}
			break;
		case BUTTON_RAISING:
			if((HAL_GetTick()- time_aux)>= ANTIREBOTE_TIME)
			{
				if(  eboard_switch()== GPIO_PIN_SET )
				{
					debounce_State_t_ = BUTTON_UP;
					time_aux = 0;
					button_state_ = false;
				}
				else
				{
					debounce_State_t_=BUTTON_DOWN;
				}
			}

			break;
		default:
			debounce_State_t_=BUTTON_UP;
			break;
	}

}

void ao_button_callbak_t_(ao_button_t* hao, bool state)
{
	button_state_ = state;
	ELOG("Callback, button: %d", button_state_);
}

void task_button(void* argument)
{
	button_state_ = false;
	ao_button_init(&ao_button_,ao_button_callbak_t_);
	debounceFSM_init();
	while(true)
	{
		debounceFSM_update();
		ao_button_send(&ao_button_, button_state_ );
		vTaskDelay(TASK_INTERVAL);
	}
}



