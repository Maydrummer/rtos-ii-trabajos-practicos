/*
 * task_button.c
 *
 *  Created on: Nov 12, 2023
 *      Author: thony
 */
//---------------------------- MEMORIA DINAMICA TP2----------------------------------------
//Integrantes: Anthony Maisincho Jivaja, Jesus Gonzales Davila

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "config.h"
#include "hal.h"
#include "driver.h"


#include "task_button.h"
#include "ao_led.h"

#ifdef USE_QL_MEMPOOL
#include "qmpool.h"
#else
#include "memory_pool.h"
#endif

extern QueueHandle_t hqueue;

#ifdef USE_QL_MEMPOOL
QMPool* const hmp;
#else
memory_pool_t* const hmp;
#endif

TickType_t pressed_start=0;
TickType_t pressed_time;
uint8_t buffer[100];



void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) //Interrupcion del sw button
{
	if(eboard_gpio_read(EBOARD_GPIO_SW)== false)
	{
		pressed_start= xTaskGetTickCount();
	}
	else
	{
		pressed_time= xTaskGetTickCount()-pressed_start;
		pressed_start=0;
	}
}



void task_button(void* argument) //tarea productora, actualiza los eventos en la cola
{
	led_event_t* p_ao_evento;
	ao_led_init(); //se inicializa el objeto activo, y se pasa la funcion que sera el callback del objeto activo
	bool cola_status; //Para hacer un assert de la creacion de la cola
	while(1)
	{
		ELOG("--------- TASK BUTTON------- \r\n");
		ELOG("Tiempo presionado: %lu",pressed_time);
		#ifdef USE_QL_MEMPOOL
			p_ao_evento = (led_event_t*)QMPool_get(hmp, 0);
		#else
			p_ao_evento = (led_event_t*)memory_pool_block_get(hmp);
		#endif

		if (NULL != p_ao_evento ) //assert de la asignacion de memoria
		{
			ELOG("Se alocaron %d bytes", sizeof(led_event_t));
	        *p_ao_evento = inicial; //acceso indirecto al contenido y le asignamos estado inicial
			procces_button(&p_ao_evento); //evento a enviar, se lo procesa en funcion del tiempo presionado del user button, se le pasa su direccion
			if(pdPASS == xQueueSend(hqueue, (void*)&p_ao_evento, 0))
			{
				ELOG("Mensaje enviado al proceso");
			}
			else
			{
				ELOG("No hay lugar en la cola para enviar el mensajes");
				#ifdef USE_QL_MEMPOOL
						  QMPool_put(hmp ,(void*)p_ao_evento);
				#else
						  memory_pool_block_put(hmp, (void*)p_ao_evento);
				#endif
				ELOG("Memoria liberada");

			}
			//ELOG(buffer);
		}
		else
		{
			ELOG("No hay lugar en la cola para enviar el mensajes");

		}


		vTaskDelay(100);
	}


}



//Funcion no tiene retorno, tiene como parametro un puntero del tipo led_event_t
void procces_button(led_event_t *evento)
{
	if ( (t_corto <= pressed_time) && (pressed_time < t_largo))
	{
		*evento= corto;

	}
	else if((t_largo <= pressed_time) && (pressed_time < t_trabado))
	{
		*evento= largo;
	}
	else if(pressed_time > t_trabado)
	{
		*evento= trabado;
	}
	else
	{
		*evento= inicial;
	}
}


