/*
 * ao_led.h
 *
 *  Created on: Nov 19, 2023
 *      Author: thony
 */

#ifndef APP_INC_AO_LED_H_
#define APP_INC_AO_LED_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "task_button.h" //Debido a que el evento productor envia su tipo de dato definido en task_button.h


void ao_led_init();

void proccess_ao_evento(led_event_t *evento_t);



#endif /* APP_INC_AO_LED_H_ */
