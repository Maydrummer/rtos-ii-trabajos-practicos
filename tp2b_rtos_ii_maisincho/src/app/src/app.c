/*
 * Copyright (c) 2023 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : app.c
 * @date   : Feb 17, 2023
 * @author : Sebastian Bedin <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

//---------------------------- MEMORIA DINAMICA TP2----------------------------------------
//Integrantes: Anthony Maisincho Jivaja, Jesus Gonzales Davila


#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "config.h"
#include "hal.h"
#include "driver.h"


#ifdef USE_QL_MEMPOOL
#include "qmpool.h"
#else
#include "memory_pool.h"
#endif

#include "ao_led.h"
#include "task_button.h"


/********************** macros and definitions *******************************/

#define QUEUE_LENGTH_            (5)
#define QUEUE_ITEM_SIZE_         (sizeof(void*))

#define MEMORY_POOL_NBLOCKS             (10)
#define MEMORY_POOL_BLOCK_SIZE          (100)
#ifdef USE_QL_MEMPOOL
QMPool memory_pool_;
static uint8_t memory_pool_memory_[MEMORY_POOL_SIZE(MEMORY_POOL_NBLOCKS, MEMORY_POOL_BLOCK_SIZE)];
#else
static memory_pool_t memory_pool_;
static uint8_t memory_pool_memory_[MEMORY_POOL_SIZE(MEMORY_POOL_NBLOCKS, MEMORY_POOL_BLOCK_SIZE)];
#endif


QueueHandle_t hqueue;

#ifdef USE_QL_MEMPOOL
QMPool* const hmp =  &memory_pool_;
#else
memory_pool_t* const hmp = &memory_pool_;
#endif

// Tarea static

// Definir el tamaño de la pila de la tarea
#define TASK_STACK_SIZE   configMINIMAL_STACK_SIZE

// Declarar la memoria estática para la tarea
StaticTask_t xTaskBuffer;
StackType_t xStack[TASK_STACK_SIZE];

void app_init(void)
{
  // drivers
  {
    eboard_init();
    ELOG("drivers init");
  }
  // services
   {
 #ifdef USE_QL_MEMPOOL
     QMPool_init(hmp, memory_pool_memory_, 1000, 100);
 #else
     memory_pool_init(hmp, memory_pool_memory_, MEMORY_POOL_NBLOCKS, MEMORY_POOL_BLOCK_SIZE);
 #endif
     ELOG("services init");
   }

   // queue
   {
     hqueue = xQueueCreate(QUEUE_LENGTH_, QUEUE_ITEM_SIZE_);
     while(NULL == hqueue)
     {
       // error
     }
   }

  // tasks
  {
	TaskHandle_t  status;
    status = xTaskCreateStatic(task_button, "task_button", TASK_STACK_SIZE, NULL,(tskIDLE_PRIORITY + 2UL), xStack,&xTaskBuffer );
    while (pdPASS != status)
    {
      // error
    }
    ELOG("tasks init");

  }
  ELOG("app init");
}

/********************** end of file ******************************************/
