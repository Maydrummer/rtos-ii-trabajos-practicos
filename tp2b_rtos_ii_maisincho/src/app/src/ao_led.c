/*
 * ao_led.c
 *
 *  Created on: Nov 19, 2023
 *      Author: thony
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "config.h"
#include "hal.h"
#include "driver.h"
#include "ao_led.h"
#ifdef USE_QL_MEMPOOL
#include "qmpool.h"
#else
#include "memory_pool.h"
#endif

extern QueueHandle_t hqueue;

#ifdef USE_QL_MEMPOOL
QMPool* const hmp;
#else
memory_pool_t* const hmp;
#endif

// Definir el tamaño de la pila de la tarea
#define TASK_STACK_SIZE   configMINIMAL_STACK_SIZE

// Declarar la memoria estática para la tarea
StaticTask_t xTaskBuffer_2;
StackType_t xStack_2[TASK_STACK_SIZE];


static void task_(void)  //el argumento de la funcion es la estructura del objeto activo
{

  led_event_t* p_ao_evento_recibir;
  while(true)
  {

    if(pdPASS == xQueueReceive(hqueue, &p_ao_evento_recibir, portMAX_DELAY)) //No queda bloqueado esperando un evento
    {
    	// procesar evento con maquina de estado
    	ELOG("--------- TASK ACTIVE OBJECT------- \r\n");
    	ELOG("Se recibio el evento en TASK ACTIVE OBJECT...");
	    ELOG("ao, led:%d", p_ao_evento_recibir);
        proccess_ao_evento(&p_ao_evento_recibir);
		#ifdef USE_QL_MEMPOOL
			  QMPool_put(hmp ,(void*)p_ao_evento_recibir);
		#else
			  memory_pool_block_put(hmp, (void*)p_ao_evento_recibir);
		#endif


    }
  }
}




void ao_led_init(void)
{
  TaskHandle_t  status;
  status = xTaskCreateStatic(task_, "task_ao_led", TASK_STACK_SIZE, NULL, tskIDLE_PRIORITY,xStack_2,&xTaskBuffer_2 ); //se crea tarea y se envia un puntero tipo cualquiera con hao

}


//Proceso de evento
void proccess_ao_evento(led_event_t *evento_t)
{
	switch (*evento_t) {
		case inicial:
			eboard_led_green(false);
			eboard_led_red(false);

			break;
		case corto:
			eboard_led_green(!eboard_gpio_read(EBOARD_GPIO_LEDG));
			eboard_led_red(false);
			break;
		case largo:
			eboard_led_red(!eboard_gpio_read(EBOARD_GPIO_LEDR));
			eboard_led_green(false);
			break;
		case trabado:
			eboard_led_green(true);
			eboard_led_red(true);
			break;
		default:
			eboard_led_green(false);
			eboard_led_red(false);
			break;
	}
}
