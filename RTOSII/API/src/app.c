/*
 * app.c
 *
 *  Created on: Dec 8, 2023
 *      Author: thony
 */
#include "app.h"
#include "main.h"
#include "cmsis_os.h"

xTaskHandle vTask_AHandle;
void vTask_A( void *pvParameters );


void appInit( void ){

    BaseType_t ret;

    /* Task A thread at priority 2 */
    ret = xTaskCreate( vTask_A,						/* Pointer to the function thats implement the task. */
					   "Task A",					/* Text name for the task. This is to facilitate debugging only. */
					   (2 * configMINIMAL_STACK_SIZE),	/* Stack depth in words. 				*/
					   NULL,						/* We are not using the task parameter.		*/
					   (tskIDLE_PRIORITY + 2UL),	/* This task will run at priority 1. 		*/
					   &vTask_AHandle );			/* We are using a variable as task handle.	*/

    /* Check the task was created successfully. */
    configASSERT( ret == pdPASS );



}


void vTask_A( void *pvParameters )
{
	INIT_LCD();
	while(1)
	{
		CLEAR_LCD();
		vTaskDelay(1000);
		STRING_LCD((uint8_t *)"Hola mundo");
		vTaskDelay(1000);

	}

}



