################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (11.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../API/src/app.c \
../API/src/driver_i2c1.c \
../API/src/driver_lcd.c \
../API/src/driver_uart.c 

OBJS += \
./API/src/app.o \
./API/src/driver_i2c1.o \
./API/src/driver_lcd.o \
./API/src/driver_uart.o 

C_DEPS += \
./API/src/app.d \
./API/src/driver_i2c1.d \
./API/src/driver_lcd.d \
./API/src/driver_uart.d 


# Each subdirectory must supply rules for building sources it contributes
API/src/%.o API/src/%.su API/src/%.cyclo: ../API/src/%.c API/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F446xx -c -I../APP -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I"C:/Users/thony/Documents/Maestria/ProgramacionMicrocontroladores/repositorio/ProgramacionMicrocontroladores/RTOSII/API" -I"C:/Users/thony/Documents/Maestria/ProgramacionMicrocontroladores/repositorio/ProgramacionMicrocontroladores/RTOSII/API/inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-API-2f-src

clean-API-2f-src:
	-$(RM) ./API/src/app.cyclo ./API/src/app.d ./API/src/app.o ./API/src/app.su ./API/src/driver_i2c1.cyclo ./API/src/driver_i2c1.d ./API/src/driver_i2c1.o ./API/src/driver_i2c1.su ./API/src/driver_lcd.cyclo ./API/src/driver_lcd.d ./API/src/driver_lcd.o ./API/src/driver_lcd.su ./API/src/driver_uart.cyclo ./API/src/driver_uart.d ./API/src/driver_uart.o ./API/src/driver_uart.su

.PHONY: clean-API-2f-src

